/**
 * Created by Neppo_antonio on 01/10/2014.
 */
define([], function() {

    var UserService = function($resource)
    {
        var uri = 'http://limitless-eyrie-6356.herokuapp.com/users';
        var User = $resource(uri + '/:id', { id: '@id' }, {
            update: {
                method: 'PUT'
            },
            persist:{
                method: 'POST'
            },
            customDelete:{
                method: 'POST'
            }
        });

        return {
            customDelete : function(id, funcaoSucesso, funcaoErro){
                User.customDelete({id:id}, {'_method':'delete'}, funcaoSucesso, funcaoErro);
            },
            getAll : function(funcaoSucesso, funcaoErro){
                $resource(uri + '.json').query({}, funcaoSucesso, funcaoErro);
            },
            get : function(id, funcaoSucesso, funcaoErro){
                User.get({ id: id }, funcaoSucesso, funcaoErro);
            },
            delete : function(id, funcaoSucesso, funcaoErro){
                $resource(uri+'/'+id).delete({}, funcaoSucesso, funcaoErro);
            },
            create : function(user, funcaoSucesso, funcaoErro){
                user.id = null;
                User.persist({}, user, funcaoSucesso, funcaoErro);
            },
            update : function (id, nome, funcaoSucesso, funcaoErro){
                User.update({}, {
                    id: id,
                    nome: id
                }, funcaoSucesso, funcaoErro);
            }

        }
    };
    return [
        '$resource',
        UserService];
});