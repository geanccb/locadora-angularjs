define( [ ], function()
{
    return function(){
        return {
            info: function (msg) {
                $.growl({
                    message: msg,
                    type: 'info',
                    offset: 100

                });
            },
            success: function (msg) {
                $.growl({
                    message: msg,
                    type: 'success',
                    offset: 100

                });
            },
            warning: function (msg) {
                $.growl({
                    message: msg,
                    type: 'warning',
                    offset: 100

                });
            },
            error: function (msg) {
                $.growl({
                    message: msg,
                    type: 'danger',
                    offset: 100

                });
            }

        }
    };
});