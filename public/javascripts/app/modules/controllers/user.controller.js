/**
 * Created by Neppo_antonio on 01/10/2014.
 */
define([], function() {

    var UserController = function($rootScope,
                                          $scope,
                                          $filter,
                                          $alert,
                                          $userService) {
        $scope.user = {};
        $scope.remove = function(id){
            $userService.delete(id, function(data){
                for(var i=0;i<$scope.users.length;i++){
                    if($scope.users[i]['id'] == id){
                        $scope.users.splice(i, 1);
                        i--;
                    }
                }
                $scope.users.push($scope.user);
            }, function(error){
                $alert.error(error.statusText);
                console.log(error);
            });
        };
        $scope.save = function(){
            $userService.create($scope.user, function(data){
                if($scope.users == undefined ||
                    $scope.users == null ||
                    $scope.users.length == 0){
                    $scope.users = [];
                }
                $scope.users.push($scope.user);
            }, function(error){
                $alert.error(error.statusText);
                console.log(error);
            });
        };
        $scope.searchAll = function(){
            $userService.getAll(function(data){
                $scope.users = data;
            }, function(error){
                console.log(error);
            });

        };
        $scope.prepareEdit = function(obj){
            $scope.user = obj;
        }


    }
    return [
        '$rootScope',
        '$scope',
        '$filter',
        '$alert',
        '$userService',
        UserController];
});