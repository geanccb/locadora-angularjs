/**
 * Created by Neppo_antonio on 02/10/2014.
 */
'use strict';

define(['angular', 'app/app'], function(angular, app) {
    return app.config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/users', {
                templateUrl: 'views/users/users.html',
                controller: 'UserController'
            })
            .when('/', {
                templateUrl: 'views/main/main.html'
            })
            .when('/login', {
                templateUrl: '/login.html'
            })
            .otherwise({
                redirectTo: '/login.html'
            });
    }]);
});
