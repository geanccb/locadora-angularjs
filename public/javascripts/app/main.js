/**
 * Created by Neppo_antonio on 01/10/2014.
 */
'use strict';

require.config({
    paths: {
        jquery: "javascripts/lib/jquery-2.1.1",
        angular: "javascripts/lib/angular",
        angularRoute: "javascripts/lib/angular-route",
        ngResource: "javascripts/lib/angular-resource",
        bootstrap: "javascripts/lib/bootstrap",
        select2: "javascripts/lib/select2",
        angularCookies: "javascripts/lib/angular-cookies",
        datepicker: "javascripts/lib/bootstrap-datetimepicker",
        filters: "javascripts/app/filters",
        app: "javascripts/app/app",
        controllers: "javascripts/app/controllers",
        services: "javascripts/app/services",
        directives: "javascripts/app/directives"
    },
    shim: {
        'angular' : {'exports' : 'angular'},
        'angularRoute': ['angular'],
        'ngRoute' : {'exports' : 'angularRoute'}
    },
    priority: [
        "angular"
    ]
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = "NG_DEFER_BOOTSTRAP!";

require( [
    'angular',
    'app',
    'javascripts/app/routes'
], function(angular, app, routes) {


    angular.bootstrap(document, [app['name']]);
    /*var $html = angular.element(document.getElementsByTagName('html')[0]);

    angular.element().ready(function() {
        angular.resumeBootstrap([app['name']]);
    });*/
});