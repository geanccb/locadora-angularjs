/**
 * Created by Neppo_antonio on 02/10/2014.
 */
'use strict';

define([
   'angular',
   'services',
   'controllers',
   'angularRoute',
   'ngResource',
   'growl'
], function (angular) {

    // Declare app level module which depends on filters, and services

    return angular.module('locadora', [
        'ngRoute',
        'ngResource',
        'locadora.services',
        'locadora.controllers'
    ]);
});
