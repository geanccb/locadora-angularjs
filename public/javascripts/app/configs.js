/**
 * Created by Neppo_antonio on 02/10/2014.
 */
'use strict';

define(['angular', 'app/app', 'jquery'], function(angular, app, $) {
    return app.config(['$locationProvider', '$httpProvider', '$sceDelegateProvider',
        function($locationProvider, $httpProvider, $sceDelegateProvider) {
            $sceDelegateProvider.resourceUrlWhitelist(['self', 'http://limitless-eyrie-6356.herokuapp.com/**']);
            /*
             * route config
             */
            $locationProvider.hashPrefix('!');
            $httpProvider.defaults.useXDomain = true;
            /* Just setting useXDomain to true is not enough. AJAX request are also
             * send with the X-Requested-With header, which indicate them as being
             * AJAX. Removing the header is necessary, so the server is not
             * rejecting the incoming request.
             */
            delete $httpProvider.defaults.headers.common['X-Requested-With'];

            $httpProvider.responseInterceptors.push('myHttpInterceptor');
            var spinnerFunction = function (data, headersGetter) {
                // todo start the spinner here
                //alert('start spinner');
                $('#envolved').show();
                return data;
            };
            $httpProvider.defaults.transformRequest.push(spinnerFunction);
    }])// register the interceptor as a service, intercepts ALL angular ajax http calls
        .factory('myHttpInterceptor', function ($q, $window) {
            return function (promise) {
                return promise.then(function (response) {
                    // do something on success
                    // todo hide the spinner
                    //alert('stop spinner');
                    $('#envolved').hide();
                    return response;

                }, function (response) {
                    // do something on error
                    // todo hide the spinner
                    //alert('stop spinner');
                    $('#envolved').hide();
                    return $q.reject(response);
                });
            };
        }).service('noty', ['$rootScope', function( $rootScope ) {

            var queue = [];

            return {
                queue: queue,
                add: function( item ) {
                    queue.push(item);
                    setTimeout(function(){
                        // remove the alert after 2000 ms
                        $('.alerts .alert').eq(0).remove();
                        queue.shift();
                    },2000);
                },
                pop: function(){
                    return this.queue.pop();
                }
            };
        }]);
});