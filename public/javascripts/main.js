/**
 * Created by Neppo_antonio on 06/10/2014.
 */
require.config({    //this configures requirejs
    paths:{
        jquery: "lib/jquery-2.1.1",
        angular: "lib/angular",
        angularRoute: "lib/angular-route",
        ngResource: "lib/angular-resource",
        services : "app/services",
        controllers : "app/controllers",
        growl: "lib/bootstrap-growl",
        bootstrap: "lib/bootstrap"
    },
    shim: {
        angular: ['jquery'],//angular depends on jquery
        ngResource:['angular'],
        toaster: ['angular', 'jquery', 'ngAnimate'],
        ngAnimate: ['jquery', 'angular'],
        'angular' : {'exports' : 'angular'},
        'bootstrap': {
            deps: ['jquery']
        },
        'angularRoute': ['angular'],
        'ngRoute' : {'exports' : 'angularRoute'},
        controllers: ['services'],
        services: ['angular', 'ngResource'],
        'growl':{
            deps: ['jquery','bootstrap']
        },
        'jquery': {
            exports: '$'
        }
    },
    priority: [
        "angular"
    ]
});


var deps = ["angular", "app/app", "app/routes", "app/configs"];

require(deps, function(angular, app, routes, configs){
    //app (app.js) and its dependencies has been loaded

    // make sure html code doesn't have "ng-app" and another directive
    // ng-controller="HomeController", else you'll get a script error complaining
    // about HomeController not defined
    angular.bootstrap(document, [app['name']]);
});